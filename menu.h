#ifndef MENU_H
#define MENU_H

#include <iostream>
#include <stdlib.h>

using namespace std;

class Menu {
    public:
        Menu() {};
        int showMain() {
        	string menu = "\n\nOPERACIONES:\n"
                  "1. -Suma de matrices\n"
                  "2. -Producto de un escalar por una matriz\n"
                  "3. -Producto de matrices\n"
                  "4. -Matriz inversa\n"
                  "5. -Indicar tipo de matriz\n"
                  "6. -Rango de la matriz\n"
                  "7. -Transpuesta de una matriz\n"
                  "8. -Crear matriz\n"
                  "0. -SALIR\n"
                  "Ingrese el numero de la op.: ";
		    cout<<menu; cin>>menu;
		    int opcionSeleccionada = atoi(menu.c_str());
		    if(opcionSeleccionada<0 || opcionSeleccionada>8)
		        cout<<"Seleccion incorrecta\n";
		    return opcionSeleccionada;
        }        
};

#endif /* MENU_H */