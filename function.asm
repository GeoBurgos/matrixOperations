global productScalarAsm               ; required for linker and NASM

section .text              ; start of the "CODE segment"

productScalarAsm: 
	   push ebp           
       mov ebp, esp        ; set up the EBP
       push ecx            ; save used registers
       push esi

       mov eax, [ebp+12]   ; number
       mov esi, [ebp+8]    ; scalar

       mul esi
       
       pop esi             ; restore used registers
       pop ecx
       pop ebp
       ret                 ; return to caller