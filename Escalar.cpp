#include <iostream>
#include <time.h>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>


using namespace std;

void create(){
    srand(time(NULL));
    int rows, columns, entero;
    char caracter, formRand, filename[30];
    cout << "Name : "; cin >> filename;
    cout << "Rows : "; cin >> rows;
    cout << "Cols : "; cin >> columns;
    cout << "Form : "; cin >> formRand;
    ofstream file(filename);
    file << rows << " " << columns << " " << formRand << "\n";
    for (int i=0; i<rows; i++){
        for (int j=0; j<columns; j++){
            if (formRand == '+'){
                entero = 32 + rand() % 133;
                cout << "Entero : " << entero << "\n";
                caracter = (char) entero;
                file << caracter;
            }
        }
        cout<<"\n\n";
        file << "\n";
    }
    file.close();
}

void datosSplit(vector<int> &result, string datos){
    int pos = 0;
    string dato = "", charLine;
    for (int i=0; i<datos.size(); i++){
        charLine = datos[i];
        if (charLine == " " || i == datos.size()-1){
            result.push_back(atoi(dato.c_str()));
            dato = "";
        }
        dato += charLine;
    }
}
/
/*-----------------------------------------------------------------------------------*/
extern "C" int productScalarAsm(int scalar, int number);   // external ASM procedure

void productScalar() {    
    string result;
    stringstream convert;
    vector<int> vectorDatos;
    int number, scalar;
    char cadena[148], charFile, filename1[30];
    cout << "Name1 : "; cin >> filename1;
    cout << "Escalar : ";cin >> scalar;
    ifstream file1(filename1);
    file1.getline(cadena, 148);
    datosSplit(vectorDatos,cadena);
    ofstream fileResult("result.txt");
    fileResult << cadena << "\n";
    cout<<"Matriz resultado: \n";
    for (int i=0; i<vectorDatos[0]; i++){
        cout<<"|";
        for (int j=0; j<vectorDatos[1]; j++){
            file1 >> charFile;
            number = (int) charFile;
            if(number < 0)
                number = 256 + number;
            number = productScalarAsm(scalar,number);
            convert.str("");
            convert << (number);
            result = convert.str();
            while(result.size() < 5)
                result+=" ";
            cout<<" "<<result;
            fileResult << number << " ";
        }
        cout<<" |\n";
        fileResult << "\n";
    }
    fileResult.close();
}




int main () {
    Menu menu;
    int opcion;
    int a[] = {1, 3, 5, 7, 9, 2, 4, 6, 8, 0};  // array declaration
    int array_length = 10;
    int sum;                                      // call of the ASM procedure
    while(true){
        cout<< "1. Crear matriz "<<endl;
        cout<< "2. producto de una matriz por un escalar "<< endl;
        cout<< "Indique la opcion "<< endl;
        cin>> opcion;
        
        switch (opcion) {
        case 1:
             create(); 
            break;
        case 2:
            productScalar();
            break;
        case 0:
            exit(1);
            break;
        default:
            break;
        }
    }
    return 1;
}
