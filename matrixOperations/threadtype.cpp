#include "threadtype.h"

ThreadType::ThreadType(QString s) : name(s){ }

void ThreadType::run() {
    int entero1;
    for (int j=0; j < row.size(); j++) {
        entero1 = (int) row[j];
        if (entero1 < 0) entero1 += 256;
        entero1 -= 132;
        if (pos == 0 && j == 0)
            *diag = entero1;
        if (entero1 != 0)
            *null = false;
        if ((pos > j && entero1 != 0) || (pos <= j && entero1 == 0))
            *lowerTri = false;
        if ((pos < j && entero1 != 0) || (pos >= j && entero1 == 0))
            *higherTri = false;
        if ((pos == j && entero1 == 0) || (pos != j && entero1 != 0))
            *diagonal = false;
        if ((pos == j && entero1 != *diag) || (pos != j && entero1 != 0))
            *scalar = false;
        if ((pos == j && entero1 != 1) || (pos != j && entero1 != 0))
            *identity = false;
    }
}
