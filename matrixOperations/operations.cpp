#include "operations.h"
//Librerias para funciones globales
#include <iostream>
#include <vector>

using namespace std;

void datosSplit(vector<int> &result, string datos){
    string dato = "", charLine;
    for (int i=0; i<datos.size(); i++){
        charLine = datos[i];
        if (charLine == " " || i == datos.size()-1){
            result.push_back(atoi(dato.c_str()));
            dato = "";
        }
        dato += charLine;
    }
}

Operations::Operations(){ }

void Operations::sum(int numThreads) {
    string result;
    vector<int> resultado, datosMatrices;
    vector<ThreadSum*> hilos;
    stringstream convert;
    char cadena[148], filename1[30], filename2[30];
    cout << "Name1 : "; cin >> filename1;
    cout << "Name2 : "; cin >> filename2;
    ifstream file1(filename1); ifstream file2(filename2);
    file1.getline(cadena, 148);
    datosSplit(datosMatrices,cadena);
    file2.getline(cadena, 148);
    datosSplit(datosMatrices,cadena);
    for (int i = 0; i < datosMatrices[0]*datosMatrices[1]; ++i ) resultado.push_back(0);
    if (numThreads != 0) {
        for (int i = 0; i < numThreads; i++) {
            cout<<"Creando hilo...\n";
            ThreadSum* th = new ThreadSum("thread"+QString::number(i));
            hilos.push_back(th);
        }
    }
    char row1[datosMatrices[1]+10], row2[datosMatrices[1]+10];
    ofstream fileResult("resultado.txt");
    fileResult << cadena << "\n";
    if (datosMatrices[0] == datosMatrices[3] && datosMatrices[1] == datosMatrices[4]) {
        string rowm1,rowm2;
        for (int i=0; i<datosMatrices[0]; i++){
            file1.getline(row1,datosMatrices[1]+10);
            file2.getline(row2,datosMatrices[1]+10);
            rowm1 = row1,rowm2 = row2;
            if (numThreads != 0) {
                ThreadSum* th = hilos[i%numThreads];
                th->row1 = row1;
                th->row2 = row2;
                th->pos = i;
                th->numColumns = datosMatrices[1];
                th->resultVector = &resultado;
                th->start();
                if (i >= numThreads) {
                    ThreadSum* th = hilos[(i+1)%numThreads];
                    th->wait();
                }
            } else {
                int entero1, entero2;
                for (int j=0; j < rowm1.size(); j++) {
                    entero1 = (int) rowm1[j];
                    entero2 = (int) rowm2[j];
                    if (entero1 < 0) entero1 += 256;
                    if (entero2 < 0) entero2 += 256;
                    entero1 -= 132;
                    entero2 -= 132;
                    resultado[i*datosMatrices[1]+j] = entero1+entero2;
                }
            }
        }
        if (numThreads != 0) {
            for (ThreadSum* th : hilos)th->wait();
            for (ThreadSum* th : hilos) th->exit(0);
        }
        for (int i = 0; i < resultado.size(); ++i){
            convert.str("");
            convert << (resultado[i]);
            result = convert.str();
            while(result.size() < 4) {
                result+=" ";
            }
            fileResult << result << " ";
            if ((i+1)%datosMatrices[1] == 0 && i != 0) fileResult << "\n";
        }
    }else{
        cout<<"ERROR: Las matrices son de diferente orden\n";
    }
}

void Operations::scalar(int numThreads){
    int escalar;
    string result;
    vector<int> resultado, datosMatrices;
    vector<ThreadScalar*> hilos;
    stringstream convert;
    char cadena[148], filename1[30];
    cout << "Name1 : "; cin >> filename1;
    cout << "Escalar : "; cin >> escalar;
    ifstream file1(filename1);
    file1.getline(cadena, 148);
    datosSplit(datosMatrices,cadena);
    for (int i = 0; i < datosMatrices[0]*datosMatrices[1]; ++i ) resultado.push_back(0);
    if (numThreads != 0) {
        for (int i = 0; i < numThreads; i++) {
            cout<<"Creando hilo...\n";
            ThreadScalar* th = new ThreadScalar("thread"+QString::number(i));
            hilos.push_back(th);
        }
    }
    char row1[datosMatrices[1]+10];
    ofstream fileResult("resultado.txt");
    fileResult << cadena << "\n";
    string rowm1;
    for (int i=0; i<datosMatrices[0]; i++){
        file1.getline(row1,datosMatrices[1]+10);
        rowm1 = row1;
        if (numThreads != 0) {
            cout<<"row : "<<row1<<" pos : "<<i<<endl;
            ThreadScalar* th = hilos[i%numThreads];
            th->row = row1;
            th->pos = i;
            th->numColumns = datosMatrices[1];
            th->scalar = escalar;
            th->resultVector = &resultado;
            th->start();
            if (i >= numThreads) {
                ThreadScalar* th = hilos[(i+1)%numThreads];
                th->wait();
            }
        } else {
            int entero1;
            for (int j=0; j < rowm1.size(); j++) {
                entero1 = (int) rowm1[j];
                if (entero1 < 0) entero1 += 256;
                entero1 -= 132;
                resultado[i*datosMatrices[1]+j] = entero1*escalar;
            }
        }
    }
    if (numThreads != 0) {
        for (ThreadScalar* th : hilos)th->wait();
        for (ThreadScalar* th : hilos) th->exit(0);
    }
    for (int i = 0; i < resultado.size(); ++i){
        convert.str("");
        convert << (resultado[i]);
        result = convert.str();
        while(result.size() < 5) {
            result+=" ";
        }
        fileResult << result << " ";
        if ((i+1)%datosMatrices[1] == 0 && i != 0) fileResult << "\n";
    }
}

void Operations::type(int numThreads){
    bool null = true, higherTri = true, lowerTri = true, diagonal = true, scalar = true, identity = true;
    int diag, number;
    vector<int> datosMatrices;
    vector<ThreadType*> hilos;
    char cadena[148], filename1[30];
    cout << "Name1 : "; cin >> filename1;
    ifstream file1(filename1);
    file1.getline(cadena, 148);
    datosSplit(datosMatrices,cadena);
    if (numThreads != 0) {
        for (int i = 0; i < numThreads; i++) {
            cout<<"Creando hilo...\n";
            ThreadType* th = new ThreadType("thread"+QString::number(i));
            hilos.push_back(th);
        }
    }
    char row1[datosMatrices[1]+10];
    string rowm1;
    if (datosMatrices[0] == 1)
        cout << "   -Matriz fila\n";
    if (datosMatrices[1] == 1)
        cout << "   -Matriz columna\n";
    if (datosMatrices[0] == datosMatrices[1]){
        cout << "   -Matriz cuadrada\n";
        for (int i=0; i<datosMatrices[0]; i++){
            file1.getline(row1,datosMatrices[1]+10);
            rowm1 = row1;
            if (numThreads != 0) {
                ThreadType* th = hilos[i%numThreads];
                th->row = row1;
                th->pos = i;
                th->numColumns = datosMatrices[1];
                th->null = &null;
                th->higherTri = &higherTri;
                th->lowerTri = &lowerTri;
                th->diagonal = &diagonal;
                th->scalar = &scalar;
                th->identity = &identity;
                th->diag = &diag;
                th->start();
                if (i >= numThreads) {
                    ThreadType* th = hilos[(i+1)%numThreads];
                    th->wait();
                }
            } else {
                int entero1;
                for (int j=0; j < rowm1.size(); j++) {
                    entero1 = (int) rowm1[j];
                    if (entero1 < 0) entero1 += 256;
                    entero1 -= 132;
                    if (i == 0 && j == 0)
                        diag = entero1;
                    if (entero1 != 0)
                        null = false;
                    if ((i > j && entero1 != 0) || (i <= j && entero1 == 0))
                        lowerTri = false;
                    if ((i < j && entero1 != 0) || (i >= j && entero1 == 0))
                        higherTri = false;
                    if ((i == j && entero1 == 0) || (i != j && entero1 != 0))
                        diagonal = false;
                    if ((i == j && entero1 != diag) || (i != j && entero1 != 0))
                        scalar = false;
                    if ((i == j && entero1 != 1) || (i != j && entero1 != 0))
                        identity = false;
                }
            }
        }
        if (numThreads != 0) for (ThreadType* th : hilos)th->wait();
        if (null)
            cout << "   -Matriz nula\n";
        if (lowerTri)
            cout << "   -Matriz tringular inferior\n";
        if (higherTri)
            cout << "   -Matriz tringular superior\n";
        if (diagonal && number != 1)
            cout << "   -Matriz diagonal\n";
        if (scalar && diag != 0 && diag != 1)
            cout << "   -Matriz escalar\n";
        if (identity)
            cout << "   -Matriz identidad\n";
    } else {
        cout << "   -Matriz rectangular\n";
    }

    if (numThreads != 0) {
        for (ThreadType* th : hilos)th->wait();
        for (ThreadType* th : hilos) th->exit(0);
    }
}


/*for (int j=0; j<datosMatrices[1]; j++){
    file1 >> charFile;
    numberM1 = (int) charFile;
    file2 >> charFile;
    numberM2 = (int) charFile;
    if(numberM1 < 0)
        numberM1 = 256 + numberM1;
    if (numberM2 < 0)
        numberM2 = 256 + numberM2;
    convert.str("");
    convert << (numberM1+numberM2);
    result = convert.str();
    while(result.size() < 3)
        result+=" ";
    fileResult << numberM1+numberM2 << " ";
}*/
