#ifndef THREADTYPE_H
#define THREADTYPE_H
//Librerias
#include <iostream>
#include <vector>
//Librerias de QT
#include <QThread>
#include <QString>
#include <QDebug>
#include <QMutex>

using namespace std;

class ThreadType;

class ThreadType : public QThread
{
public:
    // builder
    explicit ThreadType(QString);
    // methods
    void run();
    // attributes
    QString name;
    int numColumns;
    string row;
    bool *null;
    bool *higherTri;
    bool *lowerTri;
    bool *diagonal;
    bool *scalar;
    bool *identity;
    int *diag;
    int pos;
};

#endif // THREADTYPE_H
