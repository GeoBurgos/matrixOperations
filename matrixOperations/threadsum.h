#ifndef THREADSUM_H
#define THREADSUM_H
//Librerias
#include <iostream>
#include <vector>
//Librerias de QT
#include <QThread>
#include <QString>
#include <QDebug>
#include <QMutex>

using namespace std;

class ThreadSum;

class ThreadSum : public QThread
{
public:
    // builder
    explicit ThreadSum(QString);
    // methods
    void run();
    // attributes
    QString name;
    int pos;
    int numColumns;
    string row1;
    string row2;
    vector<int> *resultVector;
};

#endif // THREADSUM_H
