#ifndef THREADSCALAR_H
#define THREADSCALAR_H
//Librerias
#include <iostream>
#include <vector>
//Librerias de QT
#include <QThread>
#include <QString>
#include <QDebug>
#include <QMutex>

using namespace std;

class ThreadScalar;

class ThreadScalar : public QThread
{
public:
    // builder
    explicit ThreadScalar(QString);
    // methods
    void run();
    // attributes
    QString name;
    int pos;
    int numColumns;
    int scalar;
    string row;
    vector<int> *resultVector;

};

#endif // THREADSCALAR_H
