QT += core
QT -= gui

CONFIG += c++11

TARGET = matrixOperations
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    menu.cpp \
    operations.cpp \
    threadsum.cpp \
    threadscalar.cpp \
    threadtype.cpp

HEADERS += \
    menu.h \
    operations.h \
    threadsum.h \
    threadscalar.h \
    threadtype.h
