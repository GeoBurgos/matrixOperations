//Librerias de QT
#include <QCoreApplication>
//*Librerias de c++
#include <iostream>
#include <fstream>
#include <sstream>
//Librerias de clases
#include "menu.h"
#include "operations.h"
#include "threadsum.h"

using namespace std;

int numThreads = 0;

void create(){
    srand(time(NULL));
    int rows, columns, n;
    char caracter, formRand, filename[30];
    cout << "Name : "; cin >> filename; cout << "Rows : "; cin >> rows; cout << "Cols : "; cin >> columns; cout << "Form : "; cin >> formRand;
    ofstream file(filename);
    file << rows << " " << columns << " " << formRand << "\n";
    for (int i=0; i<rows; i++){
        for (int j=0; j<columns; j++){
            if (formRand == '+') {
                n = 132+rand()%(232-132);
                caracter = (char) (n);
            } else {
                n = 32+rand()%(232-32);
                caracter = (char) (n);
            }
            cout<<n<<endl;
            file << caracter;
        }
        file << "\n";
    }
    file.close();
}
/*
void datosSplit(vector<int> &result, string datos){
    string dato = "", charLine;
    for (int i=0; i<datos.size(); i++){
        charLine = datos[i];
        if (charLine == " " || i == datos.size()-1){
            result.push_back(atoi(dato.c_str()));
            dato = "";
        }
        dato += charLine;
    }
}

void sum(int numThreads) {
    string result;
    vector<int> resultado, datosMatrices;
    vector<ThreadSum*> hilos;
    stringstream convert;
    int numberM1, numberM2;
    char cadena[148], charFile, filename1[30], filename2[30];
    cout << "Name1 : "; cin >> filename1;
    cout << "Name2 : "; cin >> filename2;
    ifstream file1(filename1); ifstream file2(filename2);
    file1.getline(cadena, 148);
    datosSplit(datosMatrices,cadena);
    file2.getline(cadena, 148);
    datosSplit(datosMatrices,cadena);
    for (int i = 0; i < datosMatrices[0]*datosMatrices[1]; ++i ) resultado.push_back(0);
    if (numThreads != 0) {
        for (int i = 0; i < numThreads; i++) {
            cout<<"Creando hilo...\n";
            ThreadSum* th = new ThreadSum("thread"+QString::number(i));
            hilos.push_back(th);
        }
    }
    char row1[datosMatrices[1]+10], row2[datosMatrices[1]+10];
    ofstream fileResult("resultado.txt");
    fileResult << cadena << "\n";
    if (datosMatrices[0] == datosMatrices[3] && datosMatrices[1] == datosMatrices[4]) {
        string rowm1,rowm2;
        for (int i=0; i<datosMatrices[0]; i++){
            file1.getline(row1,datosMatrices[1]+10);
            file2.getline(row2,datosMatrices[1]+10);
            rowm1 = row1,rowm2 = row2;
            //cout<<"fila1 : "<<row1<<" size : "<<rowm1.size()<<endl;
            //cout<<"fila2 : "<<row2<<" size : "<<rowm2.size()<<endl;
            if (numThreads != 0) {
                cout<<"hilos\n";
                ThreadSum* th = hilos[i%numThreads];
                th->row1 = rowm1;
                th->row2 = rowm2;
                th->pos = i;
                th->numColumns = datosMatrices[1];
                th->resultVector = &resultado;
                th->start();
                if (i >= numThreads) {
                    ThreadSum* th = hilos[(i+1)%numThreads];
                    th->wait();
                }
            } else {
                cout<<"No hilos\n";
                int entero1, entero2;
                for (int j=0; j < rowm1.size(); j++) {
                    entero1 = (int) rowm1[j];
                    entero2 = (int) rowm2[j];
                    if (entero1 < 0) entero1 += 256;
                    if (entero2 < 0) entero2 += 256;
                    cout<<entero1<<endl;
                    cout<<entero2<<endl<<"----\n";
                    entero1 -= 132;
                    entero2 -= 132;
                    cout<<entero1<<endl;
                    cout<<entero2<<endl<<"++++\n";
                    resultado[i*datosMatrices[1]+j] = entero1+entero2;
                    cout<<"R[i] : "<<resultado[i]<<endl;
                }
            }
        }
        if (numThreads != 0) {
            for (ThreadSum* th : hilos)th->wait();
            for (ThreadSum* th : hilos) th->exit(0);
        }
        for (int i = 0; i < resultado.size(); ++i){
            convert.str("");
            convert << (resultado[i]);
            result = convert.str();
            while(result.size() < 4) {
                result+=" ";
            }
            fileResult << result << " ";
            if ((i+1)%datosMatrices[1] == 0 && i != 0) fileResult << "\n";
        }
    }else{
        cout<<"ERROR: Las matrices son de diferente orden\n";
    }
}
*/
int main()
{    
    Menu menu;
    Operations op;
    int opcion;
    while(true){
        opcion = menu.showMain();
        switch (opcion) {
        case 1:
            op.sum(numThreads); //Listo
            break;
        case 2:
            op.scalar(numThreads); //Listo
            break;
        case 3:
            //op.product(numThreads);
            break;
        case 4:
            //op.inverse(numThreads); //Leo esta buscando
            break;
        case 5:
            op.type(numThreads); //Listo
            break;
        case 6:
            //op.range(numThreads); //Leo esta buscando
            break;
        case 7:
            //op.transposed(numThreads);
            break;
        case 8:
            create(); //Listo
            break;
        case 9:
            cout<<"Ingrese numero de hilos a correr : ";
            cin>>numThreads;
            break;
        case 0:
            exit(1);
            break;
        default:
            break;
        }
    }
    return 1;
}
