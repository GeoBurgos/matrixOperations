#ifndef OPERATIONS_H
#define OPERATIONS_H
// Librerias de QT
#include <QString>
// Librerias
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
// Librerias de clases
#include "threadsum.h"
#include "threadscalar.h"
#include "threadtype.h"

using namespace std;

class Operations
{
public:
    // builder
    Operations();
    // methods
    void sum(int);
    void scalar(int);
    void product(int);
    void inverse(int);
    void type(int);
    void range(int);
    void transposed(int);
};

#endif // OPERATIONS_H
