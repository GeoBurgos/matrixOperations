#include "threadsum.h"

ThreadSum::ThreadSum(QString s) : name(s){ }

void ThreadSum::run(){
    int entero1, entero2;
    for (int i=0; i < row1.size(); i++) {
        entero1 = (int) row1[i];
        entero2 = (int) row2[i];
        if (entero1 < 0) entero1 += 256;
        if (entero2 < 0) entero2 += 256;
        //cout<<entero1<<endl;
        //cout<<entero2<<endl;
        entero1 -= 132;
        entero2 -= 132;
        //cout<<entero1<<endl;
        //cout<<entero2<<endl;
        //cout<<"--------\n";
        resultVector->operator [](pos*numColumns+i) = entero1+entero2;
        //cout<<"R["<<pos*numColumns+i<<"] : "<<resultVector->operator [](pos*numColumns+i)<<"\n";
    }
}
