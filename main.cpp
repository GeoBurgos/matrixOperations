#include <iostream>
#include <time.h>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include "menu.h"

using namespace std;

void create(){
    srand(time(NULL));
    int rows, columns, entero;
    char caracter, formRand, filename[30];
    cout << "Name : "; cin >> filename;
    cout << "Rows : "; cin >> rows;
    cout << "Cols : "; cin >> columns;
    cout << "Form : "; cin >> formRand;
    ofstream file(filename);
    file << rows << " " << columns << " " << formRand << "\n";
    for (int i=0; i<rows; i++){
        for (int j=0; j<columns; j++){
            if (formRand == '+'){
                entero = 32 + rand() % 133;
                cout << "Entero : " << entero << "\n";
                caracter = (char) entero;
                file << caracter;
            }
        }
        cout<<"\n\n";
        file << "\n";
    }
    file.close();
}

void datosSplit(vector<int> &result, string datos){
    int pos = 0;
    string dato = "", charLine;
    for (int i=0; i<datos.size(); i++){
        charLine = datos[i];
        if (charLine == " " || i == datos.size()-1){
            result.push_back(atoi(dato.c_str()));
            dato = "";
        }
        dato += charLine;
    }
}

/*int findType() {
    vector<int> vectorDatos;
    int number;
    char cadena[148], charFile, filename[30];
    cout << "Name1 : "; cin >> filename;
    ifstream file(filename);
    file.getline(cadena, 148);
    datosSplit(vectorDatos,cadena);
    for (int i = 0; i < vectorDatos.size(); ++i) {
        cout<<vectorDatos[i]<<"~\n";
    }
    if (vectorDatos[0] == vectorDatos[3] && vectorDatos[1] == vectorDatos[4]) {
        for (int i=0; i<vectorDatos[0]; i++){
            for (int j=0; j<vectorDatos[1]; j++){
                file1 >> charFile;
                numberM1 = (int) charFile;
                file2 >> charFile;
                numberM2 = (int) charFile;
                if(numberM1 < 0)
                    numberM1 = 256 + numberM1;
                if (numberM2 < 0)
                    numberM2 = 256 + numberM2;
            }
        }
    }else{
        cout<<"ERROR: Las matrices son de diferente orden\n";
    }
}*/


/*-----------------------------------------------------------------------------------*/
void matrixSum() {
    string result;
    stringstream convert;
    vector<int> vectorDatos;
    int numberM1, numberM2;
    char cadena[148], charFile, filename1[30], filename2[30];
    cout << "Name1 : "; cin >> filename1;
    cout << "Name2 : "; cin >> filename2;
    ifstream file1(filename1); ifstream file2(filename2);
    file1.getline(cadena, 148);
    datosSplit(vectorDatos,cadena);
    file2.getline(cadena, 148);
    datosSplit(vectorDatos,cadena);
    ofstream fileResult("result.txt");
    fileResult << cadena << "\n";    
    if (vectorDatos[0] == vectorDatos[3] && vectorDatos[1] == vectorDatos[4]) {
        cout<<"Matriz resultado: \n";
        for (int i=0; i<vectorDatos[0]; i++){
            cout<<"|";
            for (int j=0; j<vectorDatos[1]; j++){
                file1 >> charFile;
                numberM1 = (int) charFile;
                file2 >> charFile;
                numberM2 = (int) charFile;
                if(numberM1 < 0)
                    numberM1 = 256 + numberM1;
                if (numberM2 < 0)
                    numberM2 = 256 + numberM2;
                convert.str("");
                convert << (numberM1+numberM2);
                result = convert.str();
                while(result.size() < 3)
                    result+=" ";
                cout<<" "<<result;
                fileResult << numberM1+numberM2 << " ";
            }
            cout<<" |\n";
            fileResult << "\n";
        }
        fileResult.close();
    }else{
        cout<<"ERROR: Las matrices son de diferente orden\n";
    }
}

/*-----------------------------------------------------------------------------------*/
extern "C" int productScalarAsm(int scalar, int number);   // external ASM procedure

void productScalar() {    
    string result;
    stringstream convert;
    vector<int> vectorDatos;
    int number, scalar;
    char cadena[148], charFile, filename1[30];
    cout << "Name1 : "; cin >> filename1;
    cout << "Escalar : ";cin >> scalar;
    ifstream file1(filename1);
    file1.getline(cadena, 148);
    datosSplit(vectorDatos,cadena);
    ofstream fileResult("result.txt");
    fileResult << cadena << "\n";
    cout<<"Matriz resultado: \n";
    for (int i=0; i<vectorDatos[0]; i++){
        cout<<"|";
        for (int j=0; j<vectorDatos[1]; j++){
            file1 >> charFile;
            number = (int) charFile;
            if(number < 0)
                number = 256 + number;
            number = productScalarAsm(scalar,number);
            convert.str("");
            convert << (number);
            result = convert.str();
            while(result.size() < 5)
                result+=" ";
            cout<<" "<<result;
            fileResult << number << " ";
        }
        cout<<" |\n";
        fileResult << "\n";
    }
    fileResult.close();
}


/*-----------------------------------------------------------------------------------*/
void product() {

}

/*-----------------------------------------------------------------------------------*/
void inverse() {

}

/*-----------------------------------------------------------------------------------*/
void matrixType() {
    bool null = true, higherTri = true, lowerTri = true, diagonal = true, scalar = true, identity = true;
    vector<int> vectorDatos;
    int number, diag;
    char cadena[148], charFile, filename[30];
    cout << "Name1 : "; cin >> filename;
    ifstream file(filename);
    file.getline(cadena, 148);
    datosSplit(vectorDatos,cadena);
    if (vectorDatos[0] == 1)
        cout << "   -Matriz fila\n";
    if (vectorDatos[1] == 1)
        cout << "   -Matriz columna\n";
    if (vectorDatos[0] == vectorDatos[1]){
        cout << "   -Matriz cuadrada\n";
        for (int i=0; i<vectorDatos[0]; i++){
            for (int j=0; j<vectorDatos[1]; j++){
                file >> charFile;
                number = (int) charFile;
                if(number < 0)
                    number = 256 + number;
                if (i == 0 && j == 0)
                    diag = number;
                if (number != 0)
                    null = false;
                if ((i > j && number != 0) || (i <= j && number == 0))
                    lowerTri = false;
                if ((i < j && number != 0) || (i >= j && number == 0))
                    higherTri = false;
                if ((i == j && number == 0) || (i != j && number != 0))
                    diagonal = false;
                if ((i == j && number != diag) || (i != j && number != 0))
                    scalar = false;
                if ((i == j && number != 1) || (i != j && number != 0))
                    identity = false;
            }
        }
        if (null)
            cout << "   -Matriz nula\n";
        if (lowerTri)
            cout << "   -Matriz tringular inferior\n";
        if (higherTri)
            cout << "   -Matriz tringular superior\n";
        if (diagonal && number != 1)
            cout << "   -Matriz diagonal\n";
        if (scalar && diag != 0 && diag != 1)
            cout << "   -Matriz escalar\n";
        if (identity)
            cout << "   -Matriz identidad\n";
    }else{
        cout << "   -Matriz rectangular\n";
    }
}

/*-----------------------------------------------------------------------------------*/
void range() {

}

/*-----------------------------------------------------------------------------------*/
void transposed() {

}

/*-----------------------------------------------------------------------------------*/


int main () {
    Menu menu;
    int opcion;
    int a[] = {1, 3, 5, 7, 9, 2, 4, 6, 8, 0};  // array declaration
    int array_length = 10;
    int sum;                                      // call of the ASM procedure
    while(true){
        opcion = menu.showMain();
        switch (opcion) {
        case 1:
            matrixSum(); //Listo
            break;
        case 2:
            productScalar(); //Listo
            break;
        case 3:
            product();
            break;
        case 4:
            inverse(); //Leo esta buscando
            break;
        case 5:
            matrixType(); //Listo
            break;
        case 6:
            range(); //Leo esta buscando
            break;
        case 7:
            transposed();
            break;
        case 8:
            create(); //Listo
            break;
        case 0:
            exit(1);
            break;
        default:
            break;
        }
    }
    return 1;
}
